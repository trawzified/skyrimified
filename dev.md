**This message has been mirrored from the Wabbajack Discord server.**

03/21/2021
Hey everyone, unfortunately I have some bad news to share today.

I've decided to stop the Skyrimified project. It's been 3 months now and I never really realized the maintenance nightmare it can be. Both in up- and downtime, and in complexity of adjusting the setup whenever a mod goes down etc - it feels a bit like one of those cable management pictures you see of big servers sometimes. I also cannot keep up with all the mod updates in the list - there's the issue of even tracking all of them plus I'm constantly checking for conflicts on every mod with a plugin to prevent new issues from arising. And that's all without debugging the existing issues in the list. Because of all this my motivation dropped quite a bit, and I also don't have as much free time as last year. In short - I don't feel qualified anymore to be able to support the list as much as I'd like to, and it's caused me quite a bit of stress.

At the same time I feel super bad about stopping the project because I still love reading all your stories and seeing the cool screenshots, we've really built quite a nice community here. Special thanks to everyone helping to track down issues, I feel really bad about you guys helping debug stuff and perhaps not seeing a solution due to me stopping the project, but I think it's for the best. Also a special mention to @AGiantDino and @Clozure for doing lots of support when I wasn't there to help people, and all the early adopters of trawzifieds LOTD back in 2020.

What's next? I've decided to support the list for two more weeks (including updates if it goes down, unless it's an incredibly big mod) after I've published the next update and then archive it due to the reasons stated above. That's because I want to give people a chance to try Skyrimified while they can.
However, you'll definitely keep seeing me around here / the general Skyrim modding scene! I've already been working on a Discord bot to help improve the Wabbajack server here, more news about that soon. Regarding mod development, I want to focus on expanding the Synthesis patcher ecosystem to push the scene forward, and maybe even look into the details of developing SKSE64 plugins. These are the future of Skyrim modding, quite a bit easier to maintain and shouldn't require me to constantly commit such a large amount of time into them. Only thing holding them back is the amount of people that have the knowledge to work on them, so I hope I can contribute to that. Who knows, might even spend some of my time helping other (new?) list devs around here and there. There's so many amazing modlists available for Skyrim right now, really love to see it.

I was planning on sharing the new update at the same time of releasing this news, but there's actually more bad news. Auri is weaved into a ton of plugins in the modlist and I cannot easily remove her. I'm currently looking at needing to resolve > 1500 errors by hand, so I'm gonna restore from backup and try a different method. Pro tip: never let mod files depend on any merges. When will the next update be out? I hope next week, but removing Auri will be quite the job - so take everything I say about release dates with a grain of salt.

03/23/2021
Welp, guess I got lucky. Auri's back up, so list is back up too! However, this obviously means Skyrimified support will stop at the 6th of April. Grab it while you can :)

@Skyrimified

